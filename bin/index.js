#!/usr/bin/env node
const {v4: uuidv4} = require('uuid');
const dotenv = require('dotenv');
dotenv.config();
console.log("Hello this is the pubsub test script");
console.log("Usage: node . in the tools/pubsub folder");

const topicName = 'games-events-local';

// Imports the Google Cloud client library
const {PubSub} = require('@google-cloud/pubsub');

// Creates a client; cache this for further use
const pubSubClient = new PubSub();
console.log(process.env.GOOGLE_APPLICATION_CREDENTIALS);

async function publishMessage() {
    const data = JSON.stringify({
        "Timestamp": "2021-03-22T13:45:30.0000000Z",
        "Duration": 10,
        "Score": 100,
        "Level": 0
    });

    // Add two custom attributes, origin and username, to the message
    const customAttributes = {
        Player: `nick-player-${uuidv4()}`,
        Account: 'Nick',
        Event: 'SessionStarted',
        Game: 'TestRunner'
    };

    // Publishes the message as a string, e.g. "Hello, world!" or JSON.stringify(someObject)
    const dataBuffer = Buffer.from(data);

    try {
        const messageId = await pubSubClient.topic(topicName).publish(dataBuffer, customAttributes);
        console.log(`Message ${messageId} published.`);
    } catch (error) {
        console.error(`Received error while publishing: ${error.message}`);
        process.exitCode = 1;
    }
}

publishMessage();

