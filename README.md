# Pubsub test tool

## Howto
Simply run the script as follows.
```shell
cp .env.example .env # fill this with the path to your google svc.
npm install
npm run node-test-pubsub
``` 

## Google SVC
To connect to the topic you have to define a service account that has (at least) the following role: `roles/pubsub.publisher` see https://cloud.google.com/pubsub/docs/access-control


## data example
To change the current data, edit the 
```shell
{
    "Timestamp": "2021-03-22T13:45:30.0000000Z",
    "Duration": 10,
    "Score": 100,
    "Level": 0
}
```

base64 encoded string of the above object:

```shell
ewogICAgIlRpbWVzdGFtcCI6ICIyMDIxLTAzLTIyVDEzOjQ1OjMwLjAwMDAwMDBaIiwKICAgICJEdXJhdGlvbiI6IDEwLAogICAgIlNjb3JlIjogMTAwLAogICAgIkxldmVsIjogMAp9Cgo=
```

